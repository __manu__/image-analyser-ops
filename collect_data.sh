#!/bin/bash

mkdir -p "$EXPORT_STORE_DIR"

curl -d '{}' -X POST https://dev-app.zenyum.com/control-panel/v2/monitor/treatment/photo-review-submission/summary/export \
    -H 'Content-Type: application/json' \
    -H 'Host: dev-app.zenyum.com' \
    -H 'Origin: https://dev-admin.zenyum.com' \
    -H 'Referer: https://dev-admin.zenyum.com/' \
    -H 'Sec-Fetch-Site: same-site' \
    -H 'Accept: */*' \
    -H 'Sec-Fetch-Mode: cors' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    -H 'Authorization: Bearer eyJraWQiOiJFYzhJbE5SNjBRVTdPZmlEMmZKbmVBWkpmZjM0ank5azJPRGN2NnJkT01JPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJjOTFmNTA2Mi1jM2IxLTQ5ZjctYjcxNC0zOTQ0MjNmY2E1M2MiLCJhdWQiOiI0cWs2NmRvcWlpOHZrY2p1bDcxMjUwdDcxcSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjQyZTdkNjRmLTlhOTctNGVlZi1iMThiLWJkNGExN2M2MjFkZiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjQ3MzQ2MTI5LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuYXAtc291dGhlYXN0LTEuYW1hem9uYXdzLmNvbVwvYXAtc291dGhlYXN0LTFfWHBpaUhjTHhaIiwiY3VzdG9tOmlkIjoiNWU3Y2E2MmU0Y2VkZmQwMDAxZmU5NDUyIiwiY29nbml0bzp1c2VybmFtZSI6ImM5MWY1MDYyLWMzYjEtNDlmNy1iNzE0LTM5NDQyM2ZjYTUzYyIsImV4cCI6MTY0ODc1NDg0NCwiaWF0IjoxNjQ4NzUxMjQ0LCJlbWFpbCI6ImFkbWluQGtleXZhbHVlLnN5c3RlbXMifQ.coXcPlKzxlAkQlwZur99uRNWYLqsn0NBaH-F4jn18fxczh25L3rPKgfP6DFUAVmiBQFOSkglYXTIdnNIrDnXYVkWFJktGomH0JdupQ4K2RJnJcPuJ6OIeNlatngJrtP3LKta4NWIM4JNPeP8-jeYqhbj0tQwVJ62F6Sa550y9RxGW6J6O5fqX9o8bqsvEnhj6YQRHtuIFZGqZB1gzsytFAVc4dMiIh6-TB0VeRAtNopUqXqjUgU0MwkkYNUNz3pKBwGUoGozPMnNM5p3NbkYj7JXql1xyIdVex1x3g4mlg2TRrDJhUqG2uiora3D1wOKruIuIC9PtgaiQO6XkaSekA' --output "$EXPORT_STORE_PATH"