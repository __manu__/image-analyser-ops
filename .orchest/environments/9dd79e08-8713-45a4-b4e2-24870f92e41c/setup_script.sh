#!/bin/bash

# Install any dependencies you have in this shell script,
# see https://docs.orchest.io/en/latest/fundamentals/environments.html#install-packages

# E.g. mamba install -y tensorflow
sudo apt-get update
# sudo apt-get install -y tcl
# sudo apt-get install -y build-essential ffmpeg libsm6 libxext6

mamba create -y -n py3712 python=3.7.12 future
mamba run -n py3712 pip install orchest
mamba run -n py3712 pip install --upgrade pip
mamba install -y -n py3712 ipykernel jupyter_client ipython_genutils pycryptodomex future openpyxl pandas "pyarrow<=4.0.0" "h5py=2.10.0" "imgaug=0.4.0" "imutils=0.5.4" "Keras=2.2.4" "matplotlib=3.5.1" "numpy=1.21.2" "Pillow=9.0.1" "pycocotools=2.0.4" "scipy=1.6.2" "tensorflow=1.14.0"
echo "export JUPYTER_PATH=/opt/conda/envs/py3712/share/jupyter" > /home/jovyan/.bashrc
echo "export CONDA_ENV=py3712" >> /home/jovyan/.bashrc
